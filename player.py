#! /usr/bin/python
import curses, sys
from curses import KEY_RIGHT as right, KEY_LEFT as left, KEY_UP as up, KEY_DOWN as down
class Player:
    def __init__(self, color, position = (3, 3), speed = 1, direction = right):
        self.color = color
        self.positions = [position, position, position]
        self.speed = speed
        self.direction = direction
        self.alive = True

    def move(self, screen):
        if not self.alive: return
        x, y = self.positions[0]
        if self.direction in [right, left]: x += self.speed * 2 if self.direction == right else -self.speed * 2
        elif self.direction in [up, down]: y += self.speed if self.direction == down else -self.speed
        bounds = screen.getmaxyx()
        x = 1 if x < 1 else x
        x = bounds[0] - 2 if x >= bounds[0] - 1 else x
        y = 1 if y < 1 else y
        y = bounds[1] - 2 if y >= bounds[1] - 1 else y
        attributes = screen.inch(y, x) 
        if int(attributes & curses.A_COLOR) == self.color or chr(attributes & 0xff) != ' ':
			self.alive = False
			return
        self.positions.pop()
        self.positions.insert(0, (x, y))

    def render(self, screen):
        if not self.alive: return
        x0, y0 = self.positions[0]
        x1, y1 = self.positions[1]
        x2, y2 = self.positions[2]
        if y0 != y1: screen.addch(y1, x1, curses.ACS_VLINE, self.color)
        if x0 != x1: screen.addch(y0, x1, curses.ACS_HLINE, self.color)
        if y0 > y1:
            if x0 > x2: screen.addch(y1, x1, curses.ACS_URCORNER, self.color)
            elif x0 < x2: screen.addch(y1, x1, curses.ACS_ULCORNER, self.color)
        elif y0 < y1:
            if x0 > x2: screen.addch(y1, x1, curses.ACS_LRCORNER, self.color)
            elif x0 < x2: screen.addch(y1, x1, curses.ACS_LLCORNER, self.color)
        if x0 > x1:
            if y0 > y2: screen.addch(y1, x1, curses.ACS_LLCORNER, self.color)
            elif y0 < y2: screen.addch(y1, x1, curses.ACS_ULCORNER, self.color)
            screen.addch(y0, x1 + self.speed, curses.ACS_HLINE, self.color)
        elif x0 < x1:
            if y0 > y2: screen.addch(y1, x1, curses.ACS_LRCORNER, self.color)
            elif y0 < y2: screen.addch(y1, x1, curses.ACS_URCORNER, self.color)
            screen.addch(y0, x1 - self.speed, curses.ACS_HLINE, self.color)
        if self.direction == left: screen.addch(y0, x0, curses.ACS_LARROW, self.color)
        elif self.direction == right: screen.addch(y0, x0, curses.ACS_RARROW, self.color)
        elif self.direction == up: screen.addch(y0, x0, curses.ACS_UARROW, self.color)
        elif self.direction == down: screen.addch(y0, x0, 'v', self.color)

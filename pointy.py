#! /usr/bin/python -B
import curses, time, socket, json
from graphics import *
from curses import KEY_RIGHT as right, KEY_LEFT as left, KEY_UP as up, KEY_DOWN as down
from player import Player 
frameRate = 8
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('localhost', 8080))
id = int(s.recv(1))
def main():
    try:
        curses.initscr()
        screen = makeScreen()
        screen.timeout(1000 // frameRate)
        size = screen.getmaxyx()
        position = (0, 0)
        direction = right
        if id == 0:
            position = (5, 5)
            direction = right
        elif id == 1:
            position = (size[0] - 5, size[1] - 5)
            direction = left
        elif id == 2:
            position = (5, size[1] - 5)
            direction = down
        elif id == 3:
            position = (size[0] - 5, 5)
            direction = up
        player = Player(curses.color_pair(id + 1), position = position, direction = direction)
        while True:
            players = s.recv(4096).strip()
            if players: [renderEnemy(str(enemy), int(i) + 1, screen) for i, enemy in json.loads(players).iteritems() if i != str(id)]
            key = screen.getch()
            if key == 27: break
            if key in [left, right, up, down]:
                if key == right and player.direction == left: player.direction = left
                elif key == left and player.direction == right: player.direction = right
                elif key == up and player.direction == down: player.direction = down
                elif key == down and player.direction == up: player.direction = up
                else: player.direction = key
            if not player.alive:
                kill(player.color, screen)
                s.send('dead')
            else: s.send(str(player.positions[0][1]) + \
				"|" + str(player.positions[0][0]) + \
				"|" + str(screen.inch(player.positions[0][1], player.positions[0][0])) + \
				"|" + str(player.positions[1][1]) + \
				"|" + str(player.positions[1][0]) + \
				"|" + str(screen.inch(player.positions[1][1], player.positions[1][0])) + \
				"|" + str(player.positions[2][1]) + \
				"|" + str(player.positions[2][0]) + \
				"|" + str(screen.inch(player.positions[2][1], player.positions[2][0])))
            player.move(screen)
            player.render(screen)
    except Exception:
        raise
    finally:
        s.close()
        curses.endwin()

main()

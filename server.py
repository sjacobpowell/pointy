#! /usr/bin/python -B
import socket, sys, thread, curses, json
from graphics import *
if len(sys.argv) != 3:
	print '\x1b[6;31;40m' + 'usage: server port numberOfPlayers\x1b[0m'
	exit()
def main():
    try:
        clients = []
        port = int(sys.argv[1])
        numberOfPlayers = int(sys.argv[2])
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('', port))
        s.listen(numberOfPlayers)
        ipSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        ipSocket.connect(('192.0.2.0', 80))
        print >> sys.stderr, '\x1b[6;31;40mServing on %s:%s...\x1b[0m' % (ipSocket.getsockname()[0], port)
        ipSocket.close()
        while len(clients) < numberOfPlayers:
            c, client_ip = s.accept()
            c.send(str(len(clients)) + "\n")
            clients.append(c)
            print >> sys.stderr, '\x1b[6;31;40mConnected to %s:%s.\x1b[0m' % (client_ip, port)
        curses.initscr()
        screen = makeScreen()
        while True:
            if screen.getch() == 27: break
            players = {}
            for i, client in enumerate(clients): players[i] = client.recv(1024)
            for i, player in players.iteritems(): renderEnemy(player, i + 1, screen)
            for client in clients: client.send(str(json.dumps(players)) + '\n')
    except Exception:
        raise
        print '\n\x1b[6;31;40mServer stopped.\x1b[0m'
    finally:
        try:
            s.close()
            for client in clients: client.close()
            curses.endwin()
        except: pass

main()

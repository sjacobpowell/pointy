import curses
def kill(color, screen):
    size = screen.getmaxyx()
    for i in range(1, size[1] - 1):
        for j in range(1, size[0] - 1):
            if int(screen.inch(i, j) & curses.A_COLOR) == color: screen.addch(i, j, ' ')

def renderEnemy(enemy, enemyId, screen):
    if '|' not in enemy:
        kill(curses.color_pair(enemyId), screen)
        return
    y0, x0, char0, y1, x1, char1, y2, x2, char2 = map(int, enemy.split("|"))
    screen.addch(y0, x0, char0)
    screen.addch(y1, x1, char1)
    screen.addch(y2, x2, char2)
    if x0 > x1: screen.addch(y0, x1 + 1, curses.ACS_HLINE, curses.color_pair(enemyId))
    if x0 < x1: screen.addch(y0, x1 - 1, curses.ACS_HLINE, curses.color_pair(enemyId))

def makeScreen():
    size = (190, 54)
    curses.initscr()
    curses.noecho()
    curses.curs_set(0)
    curses.start_color()
    curses.use_default_colors()
    for i in range(0, curses.COLORS): curses.init_pair(i, i, -1)
    screen = curses.newwin(size[1], size[0], 0, 0)
    screen.keypad(True) 
    screen.nodelay(True) 
    screen.border(0)
    return screen
